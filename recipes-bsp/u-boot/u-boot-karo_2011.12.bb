# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-tx28 a meta layer for the Yocto Project
# Embedded Linux build platform.
#
# meta-tx28 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# meta-tx28 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with meta-tx28. If not, see <http://www.gnu.org/licenses/>.

# To build u-boot for your machine, provide the following lines in your machine
# config, replacing the assignments as appropriate for your machine.
# UBOOT_MACHINE = "tx28_config"
# UBOOT_ENTRYPOINT = "0x40100000"
# UBOOT_LOADADDRESS = "0x40100000"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=1707d6db1d42237583f50183a5651ecb \
                    file://README;beginline=1;endline=22;md5=78b195c11cb6ef63e6985140db7d7bab"

FILESDIR = "${@os.path.dirname(d.getVar('FILE',1))}/u-boot-git/${MACHINE}"

require recipes-bsp/u-boot/u-boot.inc

# This revision corresponds to the tag "v2012.07". We use the revision in order
# to avoid having to fetch it from the repo during parse
SRCREV = "740562a40fa264512d5e03d0767ad23eeaf5bda3"

PV = "v2011.12-karo+git${SRCPV}"
PR = "r1"

SRC_URI = "git://git.kernelconcepts.de/karo-tx-uboot.git;branch=karo-tx28;protocol=git \
           file://0001-Makes-the-types.h-a-relative-path.patch \
           file://0002-Fixes-a-maybe-uninitialized-error-in-cmd_nand.patch \
           file://0003-Removes-the-unused-t1-variable.patch \
          "

S = "${WORKDIR}/git"

PACKAGE_ARCH = "${MACHINE_ARCH}"
